package com.example.rafiquee.mindcoach.Retrofit;


import com.example.rafiquee.mindcoach.models.RequestNotificaton;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

//  Copyright © 2017 AirFive. All rights reserved.

public interface WebServices {

    @Headers({"Authorization: key=AIzaSyCy663icfY1pBxjNwkoA7FAF4AjOoKR1nY", "Content-Type:application/json"})
    @POST("fcm/send")
    Call<RetrofitJSONResponse> sendNotification(@Body RequestNotificaton requestNotificaton);
}