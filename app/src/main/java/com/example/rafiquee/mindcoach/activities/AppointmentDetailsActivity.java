package com.example.rafiquee.mindcoach.activities;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.telephony.PhoneNumberUtils;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rafiquee.mindcoach.R;
import com.example.rafiquee.mindcoach.Retrofit.CustomCallback;
import com.example.rafiquee.mindcoach.Retrofit.RetrofitJSONResponse;
import com.example.rafiquee.mindcoach.Retrofit.WebServicesHandler;
import com.example.rafiquee.mindcoach.fragments.AppointmentsListFragment;
import com.example.rafiquee.mindcoach.models.AppointmentItem;
import com.example.rafiquee.mindcoach.models.RequestNotificaton;
import com.example.rafiquee.mindcoach.models.SendNotificationModel;
import com.example.rafiquee.mindcoach.utils.ConstantIds;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

@SuppressLint("ValidFragment")
public class AppointmentDetailsActivity extends BaseActivity {

    @BindView(R.id.tv_call)
    TextView tvCall;
    @BindView(R.id.tv_video_call)
    TextView tvVideoCall;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_appt_duration)
    TextView tvApptDuration;
    @BindView(R.id.tv_appt_date)
    TextView tvApptDate;
    @BindView(R.id.tv_phone_num)
    TextView tvPhoneNum;

    private AppointmentItem appointmentItem;
    private DatabaseReference databaseReference;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_details);

        ButterKnife.bind(this);
        initializations();
        setValuesOnViews();
    }

    private void initializations() {
        setTitle("Appointment Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        databaseReference = FirebaseDatabase.getInstance().getReference();
        if (getIntent().hasExtra("appointment_item"))
            appointmentItem = getIntent().getParcelableExtra("appointment_item");
    }

    @OnClick(R.id.tv_video_call)
    void onTvVideoCallClick() {

        if (!appointmentItem.getStatus().equalsIgnoreCase(ConstantIds.CANCELLED)) {

            //______ check if user is clicking within appointment time ____
            if (!timeNotPassed(setupAppointmentStartDateAndTime()) && timeNotPassed(setupAppointmentEndDateAndTime())) {
                if (isMyContact(appointmentItem.getTherapistPhone()))
                    openWhatsApp(appointmentItem.getTherapistPhone());
                else
                    Toast.makeText(AppointmentDetailsActivity.this, "Please add therapist into WhatsApp contacts first!", Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(this, "You can only call during appointment Time!", Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(this, "Appointment is cancelled!", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.tv_call)
    void onTvCallClick() {
        if (!appointmentItem.getStatus().equalsIgnoreCase(ConstantIds.CANCELLED)) {

            //______ check if user is clicking within appointment time ____
            if (!timeNotPassed(setupAppointmentStartDateAndTime()) && timeNotPassed(setupAppointmentEndDateAndTime())) {
                if (isMyContact(appointmentItem.getTherapistPhone()))
                    openWhatsApp(appointmentItem.getTherapistPhone());
                else
                    Toast.makeText(AppointmentDetailsActivity.this, "Please add therapist into WhatsApp contacts first!", Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(this, "You can only call during appointment Time!", Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(this, "Appointment is cancelled!", Toast.LENGTH_SHORT).show();

    }

    @OnClick(R.id.btn_cancel_appointment)
    void onBtnCancelApptClick() {
        if (appointmentItem.getStatus().equals(ConstantIds.CANCELLED))
            Toast.makeText(this, "This appointment is cancelled already!", Toast.LENGTH_SHORT).show();
        else
            openWarningSweetDialog();
    }

    @SuppressLint("SetTextI18n")
    private void setValuesOnViews() {
        if (appointmentItem != null) {
            tvName.setText(appointmentItem.getTherapistName());
            tvAddress.setText(appointmentItem.getTherapistAddress());
            tvApptDuration.setText(getFormattedTime(appointmentItem.getTimeFrom(), "hh:mm a") + " - " +
                    getFormattedTime(appointmentItem.getTimeTo(), "hh:mm a"));
            tvApptDate.setText(getFormattedDate(appointmentItem.getDate(), "dd MMMM, yyyy"));
            tvPhoneNum.setText(appointmentItem.getTherapistPhone());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private String getFormattedTime(long time, String format) {
        Date timeFromDate = new Date(time);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.ENGLISH);
        return simpleDateFormat.format(timeFromDate);
    }

    private String getFormattedDate(long time, String format) {
        Date timeFromDate = new Date(time);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.ENGLISH);
        return simpleDateFormat.format(timeFromDate);
    }

    private void openWarningSweetDialog() {
        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Are you sure?")
                .setContentText("You would not be able to change status after cancelling it!")
                .setConfirmText("Yes")
                .showCancelButton(true)
                .setCancelText("No")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        databaseReference.child("appointments_list").child(appointmentItem.getApptId())
                                .child("status").setValue(ConstantIds.CANCELLED);

                        appointmentItem.setStatus(ConstantIds.CANCELLED);
                        AppointmentsListFragment.APPT_STATUS_UPDATED = true;
                        notifyTherapistWIthNotification();

                    }
                }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
            }
        }).show();
    }

    private void notifyTherapistWIthNotification() {
        RequestNotificaton requestNotificaton = new RequestNotificaton();
        requestNotificaton.setToken("/topics/" + appointmentItem.getTherapistId());
        requestNotificaton.setSendNotificationModel(new SendNotificationModel("Appointment Cancelled!",
                appointmentItem.getPatientName() + " has cancelled the appointment at " + getFormattedTime(appointmentItem.getTimeFrom(), "hh:mm a")));

        WebServicesHandler.instance.sendNotification(requestNotificaton, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) throws JSONException {

            }
        });
    }

    private boolean timeNotPassed(Calendar calendar) {
        Calendar now = Calendar.getInstance();

        if (now.get(Calendar.DATE) == calendar.get(Calendar.DATE)) {
            if (now.get(Calendar.HOUR_OF_DAY) == calendar.get(Calendar.HOUR_OF_DAY)) {
                if (now.get(Calendar.MINUTE) < calendar.get(Calendar.MINUTE))
                    return true;
            } else if (now.get(Calendar.HOUR_OF_DAY) < calendar.get(Calendar.HOUR_OF_DAY)) {
                return true;
            }
        } else if (now.get(Calendar.DATE) < calendar.get(Calendar.DATE))
            return true;

        return false;
    }


    private Calendar setupAppointmentStartDateAndTime() {

        String selectedDate, selectedTime;
        selectedDate = getFormattedDate(appointmentItem.getDate(), "dd:MM:yyyy");
        selectedTime = getFormattedTime(appointmentItem.getTimeFrom(), "hh:mm:a");


        Calendar wakeUpTime = Calendar.getInstance();

        String[] date = selectedDate.split(":");
        wakeUpTime.set(Calendar.YEAR, Integer.parseInt(date[2]));
        wakeUpTime.set(Calendar.MONTH, Integer.parseInt(date[1]) - 1);
        wakeUpTime.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date[0]));

        String[] time = selectedTime.split(":");
        wakeUpTime.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time[0]));
        wakeUpTime.set(Calendar.MINUTE, Integer.parseInt(time[1]));
        wakeUpTime.set(Calendar.SECOND, 00);
        wakeUpTime.set(Calendar.MILLISECOND, 0);


        if (Integer.parseInt(time[0]) > 12)
            wakeUpTime.set(Calendar.HOUR, Integer.parseInt(time[0]) - 12);
        else if (Integer.parseInt(time[0]) == 12)//HOUR value is from 0-11
            wakeUpTime.set(Calendar.HOUR, 0);
        else
            wakeUpTime.set(Calendar.HOUR, Integer.parseInt(time[0]));


        if (time[2].equalsIgnoreCase("AM"))
            wakeUpTime.set(Calendar.AM_PM, Calendar.AM);
        else
            wakeUpTime.set(Calendar.AM_PM, Calendar.PM);

        return wakeUpTime;
    }

    private Calendar setupAppointmentEndDateAndTime() {

        String selectedDate, selectedTime;
        selectedDate = getFormattedDate(appointmentItem.getDate(), "dd:MM:yyyy");
        selectedTime = getFormattedTime(appointmentItem.getTimeTo(), "hh:mm:a");


        Calendar wakeUpTime = Calendar.getInstance();

        String[] date = selectedDate.split(":");
        wakeUpTime.set(Calendar.YEAR, Integer.parseInt(date[2]));
        wakeUpTime.set(Calendar.MONTH, Integer.parseInt(date[1]) - 1);
        wakeUpTime.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date[0]));

        String[] time = selectedTime.split(":");
        wakeUpTime.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time[0]));
        wakeUpTime.set(Calendar.MINUTE, Integer.parseInt(time[1]));
        wakeUpTime.set(Calendar.SECOND, 00);
        wakeUpTime.set(Calendar.MILLISECOND, 0);


        if (Integer.parseInt(time[0]) > 12)
            wakeUpTime.set(Calendar.HOUR, Integer.parseInt(time[0]) - 12);
        else if (Integer.parseInt(time[0]) == 12)//HOUR value is from 0-11
            wakeUpTime.set(Calendar.HOUR, 0);
        else
            wakeUpTime.set(Calendar.HOUR, Integer.parseInt(time[0]));


        if (time[2].equalsIgnoreCase("AM"))
            wakeUpTime.set(Calendar.AM_PM, Calendar.AM);
        else
            wakeUpTime.set(Calendar.AM_PM, Calendar.PM);

        return wakeUpTime;
    }

    private boolean isMyContact(String phone) {

        ArrayList<String> phoneNumbersList = new ArrayList<>();

        ContentResolver cr = this.getContentResolver();
        String[] PROJECTION = new String[]{
                ContactsContract.RawContacts._ID,
                ContactsContract.CommonDataKinds.Phone.NUMBER};

        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String filter = "" + ContactsContract.Contacts.HAS_PHONE_NUMBER + " > 0";/*and " + ContactsContract.CommonDataKinds.Phone.TYPE +"=" + ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE;*/

        Cursor phoneCur = cr.query(uri, PROJECTION, filter, null, "");
        while (phoneCur.moveToNext()) {
            phoneNumbersList.add(phoneCur.getString(phoneCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
        }
        phoneCur.close();


        //compare numbers
        for (String num : phoneNumbersList) {
            if (PhoneNumberUtils.compare(num, phone))
                return true;
        }

        return false;
    }

    private void openWhatsApp(String therapistPhone) {
        therapistPhone = convertNumberIntoInternational(therapistPhone);
        try {
            Intent sendIntent = new Intent("android.intent.action.MAIN");
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.setType("text/plain");
            sendIntent.putExtra(Intent.EXTRA_TEXT, "Hi");
            sendIntent.putExtra("jid", therapistPhone + "@s.whatsapp.net");
            sendIntent.setPackage("com.whatsapp");
            startActivity(sendIntent);
        } catch (Exception e) {
            Toast.makeText(this, "Error\n" + e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    private String convertNumberIntoInternational(String number) {

        number = normalizeNum(number);

        //check if string contains only integers
        if (number.matches("^[0-9]+$")) {

            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
            Phonenumber.PhoneNumber phoneNumber = null;
            try {
                phoneNumber = phoneUtil.parse(number, "PK");
            } catch (NumberParseException e) {
                e.printStackTrace();
            }

            if (phoneNumber != null) {
                String num = phoneUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);
                return num.replaceAll("\\s+", "").substring(1);
            }
        } else
            return number.replaceAll("\\s+", "").substring(1);

        return null;
    }

    private String normalizeNum(String number) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            number = PhoneNumberUtils.normalizeNumber(number);
        } else {
            number = number.replaceAll("\\s+", "");
            number = number.replaceAll("-", "");
            number = number.replaceAll("\\(", "");
            number = number.replaceAll("\\)", "");
        }

        return number;
    }
}
