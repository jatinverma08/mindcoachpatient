package com.example.rafiquee.mindcoach.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.MenuItem;

import com.example.rafiquee.mindcoach.R;
import com.example.rafiquee.mindcoach.fragments.HireTherapistFragment;
import com.example.rafiquee.mindcoach.models.PatientInfoORM;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class HireTherapistActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hire_therapist);
        setTitle("Find Therapist");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        replaceFragment(new HireTherapistFragment(), null, false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getSupportFragmentManager().getBackStackEntryCount() >= 1) {
                    getSupportFragmentManager().popBackStack();
                } else {
                    if (getIntent().hasExtra("tag_from") && getIntent().getStringExtra("tag_from").equalsIgnoreCase("ChangeTherapistFragment")) {
                        finish();
                    } else {
                        PatientInfoORM patientInfoORM = PatientInfoORM.findById(PatientInfoORM.class, 1L);
                        if (TextUtils.isEmpty(patientInfoORM.getTherapistId()) || patientInfoORM.getTherapistId().equalsIgnoreCase(" ")) {
                            openDialogToAskForTherapistSelection();
                        } else {
                            finish();
                        }
                    }
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() >= 1) {
            getSupportFragmentManager().popBackStack();
        } else {
            if (getIntent().hasExtra("tag_from") && getIntent().getStringExtra("tag_from").equalsIgnoreCase("ChangeTherapistFragment")) {
                finish();
            } else {
                PatientInfoORM patientInfoORM = PatientInfoORM.findById(PatientInfoORM.class, 1L);
                if (TextUtils.isEmpty(patientInfoORM.getTherapistId()) || patientInfoORM.getTherapistId().equalsIgnoreCase(" ")) {
                    openDialogToAskForTherapistSelection();
                } else {
                    finish();
                }
            }
        }
    }

    private void openDialogToAskForTherapistSelection() {
        new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                .setContentText("You need to select a therapist first!")
                .setConfirmText("Ok")
                .showCancelButton(true)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                }).show();
    }
}
