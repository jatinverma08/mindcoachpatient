package com.example.rafiquee.mindcoach.activities;

import android.content.Intent;
import android.os.Bundle;

import com.example.rafiquee.mindcoach.R;
import com.example.rafiquee.mindcoach.fragments.questionnaire.StartingFragment;
import com.example.rafiquee.mindcoach.shared_prefs.SharedPreferencesUtility;
import com.example.rafiquee.mindcoach.utils.ConstantIds;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //______ don't show login page again if already logged in _______
        if (SharedPreferencesUtility.getPreference(this, ConstantIds.IS_PATIENT_LOGGED_IN, false)) {
            startActivity(new Intent(this, HomeActivity.class));
            finish();
        } else
            replaceFragment(new StartingFragment(), null, false);
    }
}
