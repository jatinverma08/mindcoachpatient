package com.example.rafiquee.mindcoach.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.webkit.WebView;

import com.example.rafiquee.mindcoach.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PrivacyPolicyActivity extends BaseActivity {

    @BindView(R.id.wv_privacy_policy)
    WebView wvPrivacyPolicy;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);
        ButterKnife.bind(this);
        initializations();
        loadPrivacyPolicy();
    }

    private void initializations() {
        setTitle("Privacy Policy");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void loadPrivacyPolicy() {
        StringBuilder builder = new StringBuilder();
        InputStream page = getResources().openRawResource(R.raw.privacy_policy);
        BufferedReader r = new BufferedReader(new InputStreamReader(page));
        try {
            String s;
            while ((s = r.readLine()) != null)
                builder.append(s);
            r.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        wvPrivacyPolicy.loadData(builder.toString(), "text/html", "UTF-8");
    }
}

