package com.example.rafiquee.mindcoach.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.webkit.WebView;

import com.example.rafiquee.mindcoach.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TipsActivity extends BaseActivity {

    @BindView(R.id.wv_tips)
    WebView wvTips;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tips);
        ButterKnife.bind(this);

        initializations();
    }

    private void initializations() {
        setTitle("Tips");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        wvTips.loadUrl("https://www.healthline.com/nutrition/16-ways-relieve-stress-anxiety");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
