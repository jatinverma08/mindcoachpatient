package com.example.rafiquee.mindcoach.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.rafiquee.mindcoach.R;
import com.example.rafiquee.mindcoach.interfaces.CustomOnClick;
import com.example.rafiquee.mindcoach.models.AppointmentItem;
import com.example.rafiquee.mindcoach.models.ApptSlotItem;
import com.example.rafiquee.mindcoach.view_holders.AppointmentSlotsViewHolder;
import com.example.rafiquee.mindcoach.view_holders.AppointmentsListViewHolder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by khalidz on 9/5/2017.
 */

public class AppointmentSlotButtonsAdapter extends RecyclerView.Adapter<AppointmentSlotsViewHolder> {

    private Context context;
    private CustomOnClick customOnClick;
    private ArrayList<ApptSlotItem> apptSlotsList;

    public AppointmentSlotButtonsAdapter() {
    }

    public AppointmentSlotButtonsAdapter(Context context, ArrayList<ApptSlotItem> apptSlotsList, CustomOnClick customOnClick) {
        this.context = context;
        this.apptSlotsList = apptSlotsList;
        this.customOnClick = customOnClick;
    }

    @Override
    public AppointmentSlotsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_appt_slot_button_item, parent, false);

        AppointmentSlotsViewHolder vh = new AppointmentSlotsViewHolder(view, apptSlotsList, customOnClick);
        return vh;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(AppointmentSlotsViewHolder holder, int position) {
        holder.btnApptSLot.setText(getFormattedTime(apptSlotsList.get(position).getTimeFrom()));
    }

    @Override
    public int getItemCount() {
        return apptSlotsList.size();
    }

    private String getFormattedTime(long timeFrom) {
        Date timeFromDate = new Date(timeFrom);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
        return simpleDateFormat.format(timeFromDate);
    }
}