package com.example.rafiquee.mindcoach.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rafiquee.mindcoach.R;
import com.example.rafiquee.mindcoach.view_holders.AvailableDaysViewHolder;

import java.util.ArrayList;

public class AvailableDaysAdapter extends RecyclerView.Adapter<AvailableDaysViewHolder> {

    private Context context;
    private ArrayList<String> availableDaysList;

    public AvailableDaysAdapter(ArrayList<String> availableDaysList) {
        this.availableDaysList = availableDaysList;
    }

    @Override
    public AvailableDaysViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_days_list_item, parent, false);

        AvailableDaysViewHolder vh = new AvailableDaysViewHolder(view, availableDaysList);
        return vh;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(AvailableDaysViewHolder holder, int position) {
        holder.tvDay.setText(availableDaysList.get(position));
    }

    @Override
    public int getItemCount() {
        return availableDaysList.size();
    }
}
