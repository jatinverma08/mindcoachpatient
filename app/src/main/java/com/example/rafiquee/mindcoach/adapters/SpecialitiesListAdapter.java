package com.example.rafiquee.mindcoach.adapters;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rafiquee.mindcoach.R;
import com.example.rafiquee.mindcoach.view_holders.SpecialitiesListViewHolder;

import java.util.ArrayList;

public class SpecialitiesListAdapter extends RecyclerView.Adapter<SpecialitiesListViewHolder> {

    private ArrayList<String> specialitiesList;

    public SpecialitiesListAdapter() {
    }

    public SpecialitiesListAdapter(ArrayList<String> specialitiesList) {
        this.specialitiesList = specialitiesList;
    }

    @Override
    public SpecialitiesListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_speciality_list_item, parent, false);

        SpecialitiesListViewHolder vh = new SpecialitiesListViewHolder(view, specialitiesList);
        return vh;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(SpecialitiesListViewHolder holder, int position) {
        holder.tvSpeciality.setText(specialitiesList.get(position));
    }

    @Override
    public int getItemCount() {
        return specialitiesList.size();
    }
}