package com.example.rafiquee.mindcoach.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.example.rafiquee.mindcoach.R;
import com.example.rafiquee.mindcoach.Retrofit.CustomCallback;
import com.example.rafiquee.mindcoach.Retrofit.RetrofitJSONResponse;
import com.example.rafiquee.mindcoach.Retrofit.WebServicesHandler;
import com.example.rafiquee.mindcoach.activities.HireTherapistActivity;
import com.example.rafiquee.mindcoach.models.PatientInfoORM;
import com.example.rafiquee.mindcoach.models.QuestionnaireORM;
import com.example.rafiquee.mindcoach.models.RequestNotificaton;
import com.example.rafiquee.mindcoach.models.SendNotificationModel;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONException;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class ChangeTherapistFragment extends BaseFragment {


    @BindView(R.id.tv_therapist_change_reason)
    TextView tvTherapistChangeReason;
    @BindView(R.id.cb_depression)
    CheckBox cbDepression;
    @BindView(R.id.cb_stress_and_anxiety)
    CheckBox cbStressAndAnxiety;
    @BindView(R.id.cb_coping_with_addictions)
    CheckBox cbCopingAddiction;
    @BindView(R.id.cb_lgbt_related_issues)
    CheckBox cbLGBTRelatedIssues;
    @BindView(R.id.cb_relationship_issues)
    CheckBox cbRelashonshipIssues;
    @BindView(R.id.cb_family_conflicts)
    CheckBox cbFamilyConflicts;
    @BindView(R.id.cb_trauma_and_abuse)
    CheckBox cbTraumaAndAbuse;
    @BindView(R.id.cb_coping_with_grief)
    CheckBox cbCopingWithGrief;
    @BindView(R.id.cb_intimacy_related_issues)
    CheckBox cbIntimicyRelatedIssues;
    @BindView(R.id.cb_eating_disorder)
    CheckBox cbEatingDisorder;
    @BindView(R.id.cb_sleeping_disorder)
    CheckBox cbSleepingDisorder;
    @BindView(R.id.cb_parenting_issues)
    CheckBox cbParentingIssues;
    @BindView(R.id.cb_motivation)
    CheckBox cbMotivation;
    @BindView(R.id.cb_anger_management)
    CheckBox cbAngerManagement;
    @BindView(R.id.cb_career_difficulties)
    CheckBox cbCareerDifficulties;
    @BindView(R.id.cb_bipolar_disorder)
    CheckBox cbBipolarDisorder;
    @BindView(R.id.cb_life_changes)
    CheckBox cbLifeChanges;
    @BindView(R.id.cb_professional_coaching)
    CheckBox cbProfessionalCoaching;
    @BindView(R.id.cb_compassion_fatigue)
    CheckBox cbCompassionFatigue;
    @BindView(R.id.cb_concentration)
    CheckBox cbConcentration;

    private DatabaseReference databaseReference;
    private PatientInfoORM patientInfoORM;
    private QuestionnaireORM questionnaireORM;
    private ArrayList<String> selectedTherapistExperiencesList = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_therapist, container, false);
        ButterKnife.bind(this, view);

        initializations(view);
        showAlreadySelected();
        return view;
    }

    private void initializations(View view) {
        patientInfoORM = PatientInfoORM.findById(PatientInfoORM.class, 1L);
        questionnaireORM = QuestionnaireORM.findById(QuestionnaireORM.class, 1L);

        databaseReference = FirebaseDatabase.getInstance().getReference();
    }

    @OnClick(R.id.btn_change_therapist)
    void onBtnChangeTherapistClick() {
        String problems = "";

        //______ update localDB _____
        for (int i = 0; i < selectedTherapistExperiencesList.size(); i++)
            problems = problems + selectedTherapistExperiencesList.get(i) + ", ";
        questionnaireORM.setProblemsToTreat(problems);
        questionnaireORM.save();

        databaseReference.child("patients_list").child(patientInfoORM.getUserId()).child("questionnaire")
                .child("problemsToTreat").setValue(problems);

        Intent intent = new Intent(getActivity(), HireTherapistActivity.class);
        intent.putExtra("tag_from", "ChangeTherapistFragment");
        startActivity(intent);
        getActivity().getSupportFragmentManager().popBackStack();
    }


    private void showAlreadySelected() {

        if (questionnaireORM.getProblemsToTreat().contains(getString(R.string.depression)))
            cbDepression.setChecked(true);
        if (questionnaireORM.getProblemsToTreat().contains(getString(R.string.stress_and_anxiety)))
            cbStressAndAnxiety.setChecked(true);
        if (questionnaireORM.getProblemsToTreat().contains(getString(R.string.coping_with_addictions)))
            cbCopingAddiction.setChecked(true);
        if (questionnaireORM.getProblemsToTreat().contains(getString(R.string.lgbt_related_issues)))
            cbLGBTRelatedIssues.setChecked(true);
        if (questionnaireORM.getProblemsToTreat().contains(getString(R.string.relationship_issues)))
            cbRelashonshipIssues.setChecked(true);
        if (questionnaireORM.getProblemsToTreat().contains(getString(R.string.family_conflicts)))
            cbFamilyConflicts.setChecked(true);
        if (questionnaireORM.getProblemsToTreat().contains(getString(R.string.trauma_and_abuse)))
            cbTraumaAndAbuse.setChecked(true);
        if (questionnaireORM.getProblemsToTreat().contains(cbIntimicyRelatedIssues.getText().toString()))
            cbIntimicyRelatedIssues.setChecked(true);
        if (questionnaireORM.getProblemsToTreat().contains(cbCopingWithGrief.getText().toString()))
            cbCopingWithGrief.setChecked(true);
        if (questionnaireORM.getProblemsToTreat().contains(cbEatingDisorder.getText().toString()))
            cbEatingDisorder.setChecked(true);
        if (questionnaireORM.getProblemsToTreat().contains(cbSleepingDisorder.getText().toString()))
            cbSleepingDisorder.setChecked(true);
        if (questionnaireORM.getProblemsToTreat().contains(cbParentingIssues.getText().toString()))
            cbParentingIssues.setChecked(true);
        if (questionnaireORM.getProblemsToTreat().contains(cbMotivation.getText().toString()))
            cbMotivation.setChecked(true);
        if (questionnaireORM.getProblemsToTreat().contains(cbAngerManagement.getText().toString()))
            cbAngerManagement.setChecked(true);
        if (questionnaireORM.getProblemsToTreat().contains(cbCareerDifficulties.getText().toString()))
            cbCareerDifficulties.setChecked(true);
        if (questionnaireORM.getProblemsToTreat().contains(cbBipolarDisorder.getText().toString()))
            cbBipolarDisorder.setChecked(true);
        if (questionnaireORM.getProblemsToTreat().contains(cbLifeChanges.getText().toString()))
            cbLifeChanges.setChecked(true);
        if (questionnaireORM.getProblemsToTreat().contains(cbProfessionalCoaching.getText().toString()))
            cbProfessionalCoaching.setChecked(true);
        if (questionnaireORM.getProblemsToTreat().contains(cbCompassionFatigue.getText().toString()))
            cbCompassionFatigue.setChecked(true);
        if (questionnaireORM.getProblemsToTreat().contains(cbConcentration.getText().toString()))
            cbConcentration.setChecked(true);

    }

    @OnCheckedChanged(R.id.cb_depression)
    void setCbDepression(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.depression));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.depression));
    }

    @OnCheckedChanged(R.id.cb_stress_and_anxiety)
    void setCbStress(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.stress_and_anxiety));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.stress_and_anxiety));
    }

    @OnCheckedChanged(R.id.cb_coping_with_addictions)
    void setCbAddiction(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.coping_with_addictions));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.coping_with_addictions));
    }

    @OnCheckedChanged(R.id.cb_lgbt_related_issues)
    void setCbLGBTIssues(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.lgbt_related_issues));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.lgbt_related_issues));
    }

    @OnCheckedChanged(R.id.cb_relationship_issues)
    void setCbRelationship(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.relationship_issues));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.relationship_issues));
    }

    @OnCheckedChanged(R.id.cb_family_conflicts)
    void setCbFamilyConflicts(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.family_conflicts));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.family_conflicts));
    }

    @OnCheckedChanged(R.id.cb_trauma_and_abuse)
    void setCbTrauma(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.trauma_and_abuse));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.trauma_and_abuse));
    }

    @OnCheckedChanged(R.id.cb_coping_with_grief)
    void setCbGrief(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.coping_with_grief_and_loss));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.coping_with_grief_and_loss));
    }

    @OnCheckedChanged(R.id.cb_intimacy_related_issues)
    void setCbIntimacyIssues(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.intimacy_related_issues));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.intimacy_related_issues));
    }

    @OnCheckedChanged(R.id.cb_eating_disorder)
    void setCbEatingDisorder(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.eating_disorder));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.eating_disorder));
    }

    @OnCheckedChanged(R.id.cb_sleeping_disorder)
    void setCbSleepingDisorder(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.sleeping_disorder));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.sleeping_disorder));
    }

    @OnCheckedChanged(R.id.cb_parenting_issues)
    void setCbParentingIssues(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.parenting_issues));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.parenting_issues));
    }

    @OnCheckedChanged(R.id.cb_motivation)
    void setCbMotivation(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.motivation_self_esteem_and_confidence));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.motivation_self_esteem_and_confidence));
    }

    @OnCheckedChanged(R.id.cb_anger_management)
    void setCbAngerManagement(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.anger_management));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.anger_management));
    }

    @OnCheckedChanged(R.id.cb_career_difficulties)
    void setCbCareerDifficulties(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.career_difficulties));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.career_difficulties));
    }

    @OnCheckedChanged(R.id.cb_bipolar_disorder)
    void setCbBipolarDisorder(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.bipolar_disorder));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.bipolar_disorder));
    }

    @OnCheckedChanged(R.id.cb_life_changes)
    void setCbLifeChanges(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.coping_with_life_changes));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.coping_with_life_changes));
    }

    @OnCheckedChanged(R.id.cb_professional_coaching)
    void setCbProfessionalCoaching(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.exclusive_and_professional_coaching));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.exclusive_and_professional_coaching));
    }

    @OnCheckedChanged(R.id.cb_compassion_fatigue)
    void setCbCompassionFatigue(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.compassion_fatigue));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.compassion_fatigue));
    }

    @OnCheckedChanged(R.id.cb_concentration)
    void setCbConcentration(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.concentration_memory_and_focus_adhd));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.concentration_memory_and_focus_adhd));
    }
}
