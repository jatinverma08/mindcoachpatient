package com.example.rafiquee.mindcoach.fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.rafiquee.mindcoach.Manifest;
import com.example.rafiquee.mindcoach.R;
import com.example.rafiquee.mindcoach.models.PatientInfoORM;
import com.example.rafiquee.mindcoach.utils.sending_email.GMailSender;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@SuppressLint("ValidFragment")
public class ForgotPasswordFragment extends BaseFragment {

    private static final int REQUEST_CODE_SMS_PERMISSION = 121;
    @BindView(R.id.et_phone_num)
    EditText etPhone;

    private PatientInfoORM patientInfoORM;
    private int code;

    @SuppressLint("ValidFragment")
    public ForgotPasswordFragment(PatientInfoORM patientInfoORM) {
        this.patientInfoORM = patientInfoORM;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forgot_password, container, false);
        ButterKnife.bind(this, view);

        initializations(view);
        return view;
    }

    private void initializations(View view) {
        etPhone.setText(patientInfoORM.getPhoneNum());
    }

    @OnClick(R.id.btn_send)
    void onBtnSendClick() {
        getPermissionsForSMS();
    }

    private void getPermissionsForSMS() {
        String[] PERMISSIONS = {
                android.Manifest.permission.SEND_SMS,
        };

        if (!hasPermissions(getActivity(), PERMISSIONS)) {
            ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, REQUEST_CODE_SMS_PERMISSION);
        } else {
            sendSMSAndOpenDialog(patientInfoORM.getPassword());
        }
    }

    public boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            sendSMSAndOpenDialog(patientInfoORM.getPassword());
        }
    }

    public void sendSMSAndOpenDialog(String password) {
        SmsManager smsManager = SmsManager.getDefault();
//        smsManager.sendTextMessage("+923053818344", null, code, null, null);
        smsManager.sendTextMessage(patientInfoORM.getPhoneNum(), null,
                "Mind Coach\n" + "Password for " + patientInfoORM.getEmail() + " is : " + password, null, null);
        Toast.makeText(getActivity(), "SMS Sent!", Toast.LENGTH_LONG).show();

        getActivity().getSupportFragmentManager().popBackStack();
    }

}
