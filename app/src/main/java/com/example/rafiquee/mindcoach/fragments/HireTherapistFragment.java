package com.example.rafiquee.mindcoach.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.rafiquee.mindcoach.R;
import com.example.rafiquee.mindcoach.adapters.TherapistsListAdapter;
import com.example.rafiquee.mindcoach.interfaces.CustomOnClick;
import com.example.rafiquee.mindcoach.models.PatientInfoORM;
import com.example.rafiquee.mindcoach.models.QuestionnaireORM;
import com.example.rafiquee.mindcoach.models.TherapistInfo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HireTherapistFragment extends BaseFragment {

    @BindView(R.id.rv_therapist_list)
    RecyclerView rvTherapistsList;

    private PatientInfoORM patientInfoORM;
    private QuestionnaireORM questionnaireORM;
    private TherapistsListAdapter therapistsListAdapter;
    private ArrayList<TherapistInfo> therapistsList = new ArrayList<>();
    private DatabaseReference databaseReference;
    private ProgressDialog progressDialog;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hire_therapist, container, false);
        ButterKnife.bind(this, view);
        initializations();

        fetchTherapistListFromFirebase();
        return view;
    }

    private void initializations() {

        patientInfoORM = PatientInfoORM.findById(PatientInfoORM.class, 1L);
        questionnaireORM = QuestionnaireORM.findById(QuestionnaireORM.class, 1L);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        rvTherapistsList.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void fetchTherapistListFromFirebase() {

        progressDialog = ProgressDialog.show(getActivity(), "", "Please wait...");
        databaseReference.child("therapists_list")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            for (DataSnapshot child : dataSnapshot.getChildren()) {

                                for (int i = 0; i < child.getValue(TherapistInfo.class).getSpecialitiesList().size(); i++) {
                                    if (questionnaireORM.getProblemsToTreat().contains(child.getValue(TherapistInfo.class).getSpecialitiesList().get(i))) {
                                        therapistsList.add(child.getValue(TherapistInfo.class));
                                        break;
                                    }
                                }
                            }

                            setupAdapter();
                        }
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (getActivity() != null) {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), databaseError.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void setupAdapter() {
        therapistsListAdapter = new TherapistsListAdapter(getActivity(), therapistsList, new CustomOnClick() {
            @Override
            public void onClick(View view, int position) {
                addFragment(new TherapistDetailsFragment(therapistsList.get(position)), null, true);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        });

        rvTherapistsList.setAdapter(therapistsListAdapter);
    }
}
