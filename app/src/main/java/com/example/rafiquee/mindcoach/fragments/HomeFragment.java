package com.example.rafiquee.mindcoach.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.rafiquee.mindcoach.R;
import com.example.rafiquee.mindcoach.activities.HireTherapistActivity;
import com.example.rafiquee.mindcoach.adapters.HomePageTabsAdapter;
import com.example.rafiquee.mindcoach.models.PatientInfoORM;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFragment extends BaseFragment {

    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.tabs)
    TabLayout tabLayout;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        initializations();
        return view;
    }

    private void initializations() {

        PatientInfoORM patientInfoORM = PatientInfoORM.findById(PatientInfoORM.class, 1L);
        if (TextUtils.isEmpty(patientInfoORM.getTherapistId()) || patientInfoORM.getTherapistId().equalsIgnoreCase(" ")) {
            Intent intent = new Intent(getActivity(), HireTherapistActivity.class);
            intent.putExtra("tag_from", "HomeFragment");
            startActivity(intent);
        }

        //______ setup adapter to show data in tabs _______
        HomePageTabsAdapter homePageTabsAdapter = new HomePageTabsAdapter(getChildFragmentManager());
        viewPager.setAdapter(homePageTabsAdapter);
        tabLayout.setupWithViewPager(viewPager);
        setUpTabs();
    }

    private void setUpTabs() {

        TextView tab1 = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.layout_tab_text, null);
        tab1.setText("Appointments");
        tabLayout.getTabAt(0).setCustomView(tab1);

        TextView tab2 = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.layout_tab_text, null);
        tab2.setText("Schedule");
        tabLayout.getTabAt(1).setCustomView(tab2);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrolled(int position, float offset, int offsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }
}
