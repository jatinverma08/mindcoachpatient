package com.example.rafiquee.mindcoach.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.rafiquee.mindcoach.R;
import com.example.rafiquee.mindcoach.activities.HomeActivity;
import com.example.rafiquee.mindcoach.models.PatientInfo;
import com.example.rafiquee.mindcoach.models.PatientInfoORM;
import com.example.rafiquee.mindcoach.models.QuestionnaireORM;
import com.example.rafiquee.mindcoach.shared_prefs.SharedPreferencesUtility;
import com.example.rafiquee.mindcoach.utils.ConstantIds;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginFragment extends BaseFragment {

    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_password)
    EditText etPassword;

    private PatientInfo patientInfo;
    private DatabaseReference databaseReference;
    private ProgressDialog progressDialog;
    private PatientInfoORM patientInfoORM;
    private static final int REQUEST_CODE_ALL_PERMISSION = 323;


    @SuppressLint("ValidFragment")
    public LoginFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);

        initializations(view);
        return view;
    }

    private void initializations(View view) {
        patientInfo = new PatientInfo();
        databaseReference = FirebaseDatabase.getInstance().getReference();
    }

    @OnClick(R.id.btn_login)
    void onBtnLoginClick() {
        if (isConnectedToInternet()) {
            if (isValid()) {
                doLogin();
            }
        } else
            Snackbar.make(getView(), "Internet connection problem!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
    }

    @OnClick(R.id.tv_sign_up)
    void onTvSignupClick() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @OnClick(R.id.tv_forgot_password)
    void onTvForgotPasswordClick() {
        if (!TextUtils.isEmpty(etEmail.getText().toString())) {
            validateEmail();
        } else {
            Snackbar.make(getView(), "Please enter email first!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
        }
    }

    private boolean isValid() {
        if (TextUtils.isEmpty(etEmail.getText().toString())) {
            Snackbar.make(getView(), "Please enter email!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            return false;
        } else if (TextUtils.isEmpty(etPassword.getText().toString())) {
            Snackbar.make(getView(), "Please enter password!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            return false;
        }

        return true;
    }

    private void doLogin() {

        progressDialog = ProgressDialog.show(getActivity(), "", "Logging in...");

        databaseReference.child("patients_list").orderByChild("email").equalTo(etEmail.getText().toString())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            for (DataSnapshot data : dataSnapshot.getChildren())
                                patientInfo = data.getValue(PatientInfo.class);

                            if (TextUtils.equals(etPassword.getText().toString(), patientInfo.getPassword())) {
                                Toast.makeText(getActivity(), "User successfully logged in!", Toast.LENGTH_SHORT).show();
                                setupLocalDB();
                                getAllPermissions();
                            } else {
                                Snackbar.make(getView(), "Password is incorrect!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            }
                        } else {
                            Snackbar.make(getView(), "This email does not exists!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        }
                        progressDialog.dismiss();
                    }

                    private void setupLocalDB() {
                        SharedPreferencesUtility.setPreference(getActivity(), ConstantIds.IS_PATIENT_LOGGED_IN, true);

                        PatientInfoORM patientInfoORM = PatientInfoORM.findById(PatientInfoORM.class, 1L);
                        if (patientInfoORM == null || !patientInfoORM.getEmail().equals(etEmail.getText().toString())) {
                            if (patientInfoORM == null)
                                patientInfoORM = new PatientInfoORM();
                            patientInfoORM.setUserId(patientInfo.getUserId());
                            patientInfoORM.setName(patientInfo.getName());
                            patientInfoORM.setPicUrl(patientInfo.getPicUrl());
                            patientInfoORM.setEmail(patientInfo.getEmail());
                            patientInfoORM.setPassword(patientInfo.getPassword());
                            patientInfoORM.setGender(patientInfo.getGender());
                            patientInfoORM.setPhoneNum(patientInfo.getPhoneNum());
                            patientInfoORM.setCountry(patientInfo.getCountry());
                            patientInfoORM.setAddress(patientInfo.getAddress());
                            patientInfoORM.setTherapistId(patientInfo.getTherapistId());
                            patientInfoORM.save();

                            QuestionnaireORM questionnaireORM = QuestionnaireORM.findById(QuestionnaireORM.class, 1L);
                            if (questionnaireORM == null)
                                questionnaireORM = new QuestionnaireORM();
                            questionnaireORM.setGender(patientInfo.getQuestionnaire().getGender());
                            questionnaireORM.setAge(patientInfo.getQuestionnaire().getAge());
                            questionnaireORM.setReligion(patientInfo.getQuestionnaire().getReligion());
                            questionnaireORM.setSuicidePlanTime(patientInfo.getQuestionnaire().getSuicidePlanTime());
                            questionnaireORM.setFinancialStatus(patientInfo.getQuestionnaire().getFinancialStatus());
                            questionnaireORM.setSleepingHabitsStatus(patientInfo.getQuestionnaire().getSleepingHabitsStatus());
                            questionnaireORM.setCountry(patientInfo.getQuestionnaire().getCountry());
                            questionnaireORM.setReligious(patientInfo.getQuestionnaire().isReligious());
                            questionnaireORM.setHasTherapist(patientInfo.getQuestionnaire().isHasTherapist());
                            questionnaireORM.setHasSadnessAndDepression(patientInfo.getQuestionnaire().isHasSadnessAndDepression());
                            questionnaireORM.setHasPanicAttacks(patientInfo.getQuestionnaire().isHasPanicAttacks());
                            questionnaireORM.setTakingMedication(patientInfo.getQuestionnaire().isTakingMedication());
                            questionnaireORM.setHasChronicPain(patientInfo.getQuestionnaire().isHasChronicPain());
                            questionnaireORM.setProblemsToTreat(patientInfo.getQuestionnaire().getProblemsToTreat());
                            questionnaireORM.save();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (getActivity() != null) {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), databaseError.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void validateEmail() {

        progressDialog = ProgressDialog.show(getActivity(), "Please wait...", "");

        //______ check if email exists in database _______
        databaseReference.child("patients_list").orderByChild("email").equalTo(etEmail.getText().toString())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            for (DataSnapshot data : dataSnapshot.getChildren())
                                patientInfoORM = data.getValue(PatientInfoORM.class);

                            replaceFragment(new ForgotPasswordFragment(patientInfoORM), null, true);
                        } else {
                            Snackbar.make(getView(), "This email is not registered!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        }
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (getActivity() != null) {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), databaseError.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void getAllPermissions() {
        String[] PERMISSIONS = {
                Manifest.permission.WRITE_CONTACTS,
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.INTERNET,
                android.Manifest.permission.SEND_SMS,
        };

        if (!hasPermissions(getActivity(), PERMISSIONS)) {
            requestPermissions(PERMISSIONS, REQUEST_CODE_ALL_PERMISSION);
        } else {
            startActivity(new Intent(getActivity(), HomeActivity.class));
            getActivity().finish();
        }
    }

    public boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ALL_PERMISSION:
                for (int permission : grantResults) {
                    if (permission == PackageManager.PERMISSION_DENIED) {
                        return;
                    }
                }

                startActivity(new Intent(getActivity(), HomeActivity.class));
                getActivity().finish();
                break;
        }
    }
}