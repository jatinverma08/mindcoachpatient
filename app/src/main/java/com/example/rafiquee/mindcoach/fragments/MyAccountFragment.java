package com.example.rafiquee.mindcoach.fragments;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.rafiquee.mindcoach.R;
import com.example.rafiquee.mindcoach.models.PatientInfoORM;
import com.example.rafiquee.mindcoach.utils.ConstantIds;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

public class MyAccountFragment extends BaseFragment {

    private static final int Image_Request_Code = 1231;
    @BindView(R.id.iv_my_pic)
    ImageView ivMyPic;
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_address)
    EditText etAddress;
    @BindView(R.id.et_phone_num)
    EditText etPhone;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.et_confirm_password)
    EditText etConfirmPassword;
    @BindView(R.id.rg_gender)
    RadioGroup rgGender;
    @BindView(R.id.rb_male)
    RadioButton rbMale;
    @BindView(R.id.rb_female)
    RadioButton rbFemale;

    private PatientInfoORM patientInfoORM;
    private Uri selectedImagePathUri;
    private DatabaseReference firebaseDatabase;
    private ProgressDialog progressDialog;
    private StorageReference storageReference;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_account, container, false);
        ButterKnife.bind(this, view);
        initializations(view);
        setValuesOnViews();
        return view;
    }

    private void initializations(View view) {
        patientInfoORM = PatientInfoORM.findById(PatientInfoORM.class, 1L);
        firebaseDatabase = FirebaseDatabase.getInstance().getReference();
        storageReference = FirebaseStorage.getInstance().getReference();
    }

    private void setValuesOnViews() {
        setPictureOnImageView();
        etName.setText(patientInfoORM.getName());
        etEmail.setText(patientInfoORM.getEmail());
        etPhone.setText(patientInfoORM.getPhoneNum());
        etAddress.setText(patientInfoORM.getAddress());
        etPassword.setText(patientInfoORM.getPassword());
        etConfirmPassword.setText(patientInfoORM.getPassword());

        if (patientInfoORM.getGender().equalsIgnoreCase("Male"))
            rbMale.setChecked(true);
        else
            rbFemale.setChecked(true);
    }

    @OnClick(R.id.rb_male)
    void OnRbMaleClick() {
        patientInfoORM.setGender(getString(R.string.male));
    }

    @OnClick(R.id.rb_female)
    void OnRbFemaleClick() {
        patientInfoORM.setGender(getString(R.string.female));
    }

    @OnClick(R.id.iv_my_pic)
    void onMyPicClick() {
        chooseProfilePictureFromStorage();
    }

    @OnClick(R.id.btn_save_changes)
    void onBtnSaveChangesClick() {
        if (isValid()) {
            if (selectedImagePathUri != null) {
                uploadImageFileToFirebaseStorage();
            } else {
                updateFirebase("");
            }
        }
    }

    private boolean isValid() {

        if (TextUtils.isEmpty(etName.getText().toString())) {
            Snackbar.make(getView(), "Please enter name!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            return false;
        } else if (TextUtils.isEmpty(etEmail.getText().toString())) {
            Snackbar.make(getView(), "Please enter email!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            return false;
        } else if (TextUtils.isEmpty(etPassword.getText().toString())) {
            Snackbar.make(getView(), "Please enter password!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            return false;
        } else if (TextUtils.isEmpty(etConfirmPassword.getText().toString())) {
            Snackbar.make(getView(), "Please confirm password!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            return false;
        }


        //____ phone validation ______
        if (!TextUtils.isEmpty(etPhone.getText().toString())) {
            String phone = etPhone.getText().toString();

            if (phone.startsWith("64") || phone.startsWith("+1")) {
                if (phone.startsWith("64") && !phone.matches(ConstantIds.CAD_NATIONAL_MOBILE_FORMAT_1) && !phone.matches(ConstantIds.CAD_NATIONAL_MOBILE_FORMAT_2)) {
                    Snackbar.make(getView(), "Please enter valid phone number!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    return false;
                } else if (phone.startsWith("+1") && !phone.matches(ConstantIds.CAD_INTERNATIONAL_MOBILE_FORMAT_1)
                        && !phone.matches(ConstantIds.CAD_INTERNATIONAL_MOBILE_FORMAT_2) && !phone.matches(ConstantIds.CAD_INTERNATIONAL_MOBILE_FORMAT_3)) {
                    Snackbar.make(getView(), "Please enter valid phone number!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    return false;
                }
            } else {
                Snackbar.make(getView(), "Please enter valid phone number!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                return false;
            }
        }

        //____ password validation ______
        if (!TextUtils.isEmpty(etPassword.getText().toString())) {
            String password = etPassword.getText().toString();
            if (password.length() < 6) {
                Snackbar.make(getView(), "Password length should be 6 at least!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                return false;
            }
            if (password.matches("^[a-zA-Z]*$") || password.matches("^[0-9]+$")) {
                Snackbar.make(getView(), "Password should contains digits and alphabets!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                return false;
            }
        }

        if (!TextUtils.equals(etPassword.getText().toString(), etConfirmPassword.getText().toString())) {
            Snackbar.make(getView(), "Password does not match!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            return false;
        }

        return true;
    }

    private void setPictureOnImageView() {
        if (TextUtils.isEmpty(patientInfoORM.getPicUrl())) {
            ivMyPic.setPadding(0, 0, 0, 0);
            ivMyPic.setImageResource(R.drawable.blank_image);
        } else {
            Glide.with(getActivity()).load(patientInfoORM.getPicUrl())
                    .apply(new RequestOptions()
                            .centerCrop()
                            .override(300, 200))
                    .into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                            ivMyPic.setPadding(0, 0, 0, 0);
                            ivMyPic.setImageDrawable(resource);
                        }
                    });
        }
    }

    private void chooseProfilePictureFromStorage() {

        Intent intent = new Intent();

        // Setting intent type as image to select image from phone storage.
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Please Select Image"), Image_Request_Code);
    }

    public String GetFileExtension(Uri uri) {

        ContentResolver contentResolver = getActivity().getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();

        // Returning the file Extension.
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Image_Request_Code && resultCode == RESULT_OK && data != null && data.getData() != null) {

            selectedImagePathUri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImagePathUri);
                ivMyPic.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void uploadImageFileToFirebaseStorage() {

        // Checking whether selectedImagePathUri Is empty or not.
        if (selectedImagePathUri != null) {

            progressDialog = ProgressDialog.show(getActivity(), "", "Picture is uploading...");

            StorageReference storageReference2nd = storageReference.child("mind_coach_pictures").child(System.currentTimeMillis() + "." + GetFileExtension(selectedImagePathUri));

            // Adding addOnSuccessListener to second StorageReference.
            storageReference2nd.putFile(selectedImagePathUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            if (taskSnapshot.getMetadata() != null) {

                                if (taskSnapshot.getMetadata().getReference() != null) {
                                    Task<Uri> result = taskSnapshot.getStorage().getDownloadUrl();
                                    result.addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri downloadUri) {
                                            progressDialog.dismiss();
                                            updateFirebase(downloadUri.toString());
                                        }
                                    });
                                }
                            }
                        }
                    })
                    // If something goes wrong .
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })

                    // On progress change upload time.
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
                        }
                    });
        } else {
            Snackbar.make(getView(), "Please select picture first!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
        }
    }

    private void updateFirebase(String downloadUri) {
        if (!TextUtils.isEmpty(downloadUri))
            patientInfoORM.setPicUrl(downloadUri);
        patientInfoORM.setName(etName.getText().toString());
        patientInfoORM.setEmail(etEmail.getText().toString());
        patientInfoORM.setPhoneNum(etPhone.getText().toString());
        patientInfoORM.setAddress(etAddress.getText().toString());
        patientInfoORM.setPassword(etPassword.getText().toString());
        patientInfoORM.save();

        if (!TextUtils.isEmpty(downloadUri))
            firebaseDatabase.child("patients_list").child(patientInfoORM.getUserId()).child("picUrl").setValue(downloadUri);
        firebaseDatabase.child("patients_list").child(patientInfoORM.getUserId()).child("name").setValue(etName.getText().toString());
        firebaseDatabase.child("patients_list").child(patientInfoORM.getUserId()).child("phoneNum").setValue(etPhone.getText().toString());
        firebaseDatabase.child("patients_list").child(patientInfoORM.getUserId()).child("address").setValue(etAddress.getText().toString());
        firebaseDatabase.child("patients_list").child(patientInfoORM.getUserId()).child("password").setValue(etPassword.getText().toString());
        firebaseDatabase.child("patients_list").child(patientInfoORM.getUserId()).child("gender").setValue(patientInfoORM.getGender());

        sendBroadcast();
        getActivity().getSupportFragmentManager().popBackStack();
    }

    private void sendBroadcast() {

        //______ update info in drader_view of drawer layout ______
        Intent intent = new Intent();
        intent.setAction(ConstantIds.HEADER_UPDATE);
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }
}
