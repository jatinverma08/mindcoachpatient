package com.example.rafiquee.mindcoach.fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rafiquee.mindcoach.R;
import com.example.rafiquee.mindcoach.Retrofit.CustomCallback;
import com.example.rafiquee.mindcoach.Retrofit.RetrofitJSONResponse;
import com.example.rafiquee.mindcoach.Retrofit.WebServicesHandler;
import com.example.rafiquee.mindcoach.adapters.AppointmentSlotButtonsAdapter;
import com.example.rafiquee.mindcoach.interfaces.CustomOnClick;
import com.example.rafiquee.mindcoach.models.AppointmentItem;
import com.example.rafiquee.mindcoach.models.ApptSlotItem;
import com.example.rafiquee.mindcoach.models.PatientInfoORM;
import com.example.rafiquee.mindcoach.models.RequestNotificaton;
import com.example.rafiquee.mindcoach.models.SendNotificationModel;
import com.example.rafiquee.mindcoach.models.TherapistInfo;
import com.example.rafiquee.mindcoach.utils.ConstantIds;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class ScheduleAppointmentFragment extends BaseFragment {

    @BindView(R.id.rv_appts_slot)
    RecyclerView rvAppointmentSlots;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_degrees)
    TextView tvDegrees;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_available_timing)
    TextView tvAvailableTiming;
    @BindView(R.id.tv_consultation_fee)
    TextView tvConsultationFee;
    @BindView(R.id.tv_day)
    TextView tvDay;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.ib_calendar)
    ImageButton ibCalendar;

    private PatientInfoORM patientInfoORM;
    private TherapistInfo myTherapist;
    private DatabaseReference databaseReference;
    private ArrayList<ApptSlotItem> apptSlotsList = new ArrayList<>();
    private AppointmentSlotButtonsAdapter appointmentSlotButtonsAdapter;
    private ArrayList<AppointmentItem> appointmentsList = new ArrayList<>();
    private ProgressDialog progressDialog;


    //______ this function will call everytime fragment will be in display _____
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    getActivity().setTitle("Schedule Appointment");
                }
            }, 500);

        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_schedule_appointments, container, false);
        ButterKnife.bind(this, view);

        initializations(view);
        return view;
    }

    private void initializations(View view) {
        populateCurrentDateAndDay();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        patientInfoORM = PatientInfoORM.findById(PatientInfoORM.class, 1L);
        fetchTherapistProfile(patientInfoORM.getTherapistId());
    }


    @OnClick(R.id.ib_calendar)
    void onIbCalendarClick() {
        openDatePickerPopup();
    }

    private void fetchTherapistProfile(String therapistId) {
        if (isConnectedToInternet()) {
            databaseReference.child("therapists_list").orderByChild("therapistId").equalTo(therapistId)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                for (DataSnapshot data : dataSnapshot.getChildren())
                                    myTherapist = data.getValue(TherapistInfo.class);

                                setValuesOnViews();
                                fetchApptSlotsFromFirebase(tvDay.getText().toString());
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            if (getActivity() != null) {
                                Toast.makeText(getActivity(), databaseError.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        } else
            Toast.makeText(getActivity(), "Internet connection problem!", Toast.LENGTH_SHORT).show();
    }

    private void fetchApptSlotsFromFirebase(String day) {
        apptSlotsList.clear();
        databaseReference.child("appointments_slots").child(myTherapist.getTherapistId()).child(day.toLowerCase())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            for (DataSnapshot data : dataSnapshot.getChildren())
                                apptSlotsList.add(data.getValue(ApptSlotItem.class));
                        }

                        setupApptSlotsAdapter();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (getActivity() != null) {
                            Toast.makeText(getActivity(), databaseError.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @SuppressLint("SetTextI18n")
    private void setValuesOnViews() {
        tvName.setText(myTherapist.getName());
        tvDegrees.setText(myTherapist.getDegrees());
        tvAddress.setText(myTherapist.getAddress());
        tvAvailableTiming.setText("Available: " + getFormattedTime(myTherapist.getAvailableTimeFrom()) + " - " + getFormattedTime(myTherapist.getAvailableTimeTo()));
        tvConsultationFee.setText("Free");
    }

    private void setupApptSlotsAdapter() {
        rvAppointmentSlots.setLayoutManager(new GridLayoutManager(getContext(), 3));
        appointmentSlotButtonsAdapter = new AppointmentSlotButtonsAdapter(getActivity(), apptSlotsList, new CustomOnClick() {
            @Override
            public void onClick(View view, int position) {
                openApptConfirmationSweetDialog(position);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        });

        rvAppointmentSlots.setAdapter(appointmentSlotButtonsAdapter);
    }


    private void openDatePickerPopup() {
        Calendar now = Calendar.getInstance();

        DatePickerDialog dpd = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

                        String date = dayOfMonth + " " + getMonthName(monthOfYear) + "," + " " + year;

                        try {
                            DateFormat dateFormat = new SimpleDateFormat("dd MMMM, yyyy", Locale.ENGLISH);
                            final Date dateTemp = dateFormat.parse(date);

                            final Calendar calendar = Calendar.getInstance();
                            calendar.setTime(dateTemp);

                            String dayLongName = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());
                            tvDay.setText(dayLongName);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        tvDate.setText(date);
                        fetchApptSlotsFromFirebase(tvDay.getText().toString());
                    }
                },
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setThemeDark(false);
        dpd.setAccentColor(getResources().getColor(R.color.blue_button_color));
        dpd.setMinDate(Calendar.getInstance());

        try {
            dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private long getDateInMillisToSaveAppointment(String date) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("d MMMM, yyyy", Locale.ENGLISH);
            Date newApptDate = simpleDateFormat.parse(date);
            return newApptDate.getTime();
        } catch (Exception ignored) {
        }

        return 0;
    }

    private String getMonthName(int month) {
        switch (month + 1) {
            case 1:
                return "Jan";

            case 2:
                return "Feb";

            case 3:
                return "Mar";

            case 4:
                return "Apr";

            case 5:
                return "May";

            case 6:
                return "Jun";

            case 7:
                return "Jul";

            case 8:
                return "Aug";

            case 9:
                return "Sep";

            case 10:
                return "Oct";

            case 11:
                return "Nov";

            case 12:
                return "Dec";
        }

        return "";
    }

    private String getFormattedTime(long time) {
        Date timeFromDate = new Date(time);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
        return simpleDateFormat.format(timeFromDate);
    }

    private void openApptConfirmationSweetDialog(final int position) {
        new SweetAlertDialog(getContext(), SweetAlertDialog.BUTTON_CONFIRM)
                .setContentText("Appointment will be scheduled with " + myTherapist.getName() + " on " + tvDate.getText())
                .setConfirmText("Ok")
                .showCancelButton(true)
                .setCancelText("Cancel")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        fetchMyBookedAppointmentFromFirebase(position);
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                    }
                }).show();
    }

    private void openApptErrorSweetDialog(final int position) {
        new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                .setContentText("This appointment slot is already booked!")
                .setConfirmText("Ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                })
                .show();
    }


    private void fetchMyBookedAppointmentFromFirebase(final int position) {
        progressDialog = ProgressDialog.show(getActivity(), "", "Please wait...");
        databaseReference.child("appointments_list").orderByChild("patientId").equalTo(patientInfoORM.getUserId())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            appointmentsList.clear();
                            for (DataSnapshot data : dataSnapshot.getChildren()) {
                                appointmentsList.add(data.getValue(AppointmentItem.class));
                            }

                            if (slotAlreadyBooked())
                                openApptErrorSweetDialog(position);
                            else
                                bookAppointment();
                        } else {
                            bookAppointment();
                        }

                        progressDialog.dismiss();
                    }

                    private boolean slotAlreadyBooked() {
                        for (int i = 0; i < appointmentsList.size(); i++) {
                            if (getDateInMillisToSaveAppointment(tvDate.getText().toString()) == appointmentsList.get(i).getDate()
                                    && appointmentsList.get(i).getTimeFrom() == apptSlotsList.get(position).getTimeFrom()
                                    && appointmentsList.get(i).getStatus().equalsIgnoreCase("PENDING")) {
                                return true;
                            }
                        }

                        return false;
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (getActivity() != null) {
                            Toast.makeText(getActivity(), databaseError.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    private void bookAppointment() {
                        AppointmentItem appointmentItem = new AppointmentItem();
                        appointmentItem.setTherapistId(myTherapist.getTherapistId());
                        appointmentItem.setTherapistName(myTherapist.getName());
                        appointmentItem.setTherapistPhone(myTherapist.getPhoneNum());
                        appointmentItem.setTherapistAddress(myTherapist.getAddress());
                        appointmentItem.setPatientId(patientInfoORM.getUserId());
                        appointmentItem.setPatientName(patientInfoORM.getName());
                        appointmentItem.setDate(getDateInMillisToSaveAppointment(tvDate.getText().toString()));
                        appointmentItem.setTimeFrom(apptSlotsList.get(position).getTimeFrom());
                        appointmentItem.setTimeTo(apptSlotsList.get(position).getTimeTo());
                        appointmentItem.setStatus(ConstantIds.PENDING);

                        String apptId = databaseReference.child("appointments_list").push().getKey();
                        appointmentItem.setApptId(apptId);

                        databaseReference.child("appointments_list").child(apptId).setValue(appointmentItem);
//                        sendApptListChangeBroadcast(appointmentItem);
                        notifyTherapistWIthNotification(appointmentItem);
                    }

                    private void sendApptListChangeBroadcast(AppointmentItem appointmentItem) {
                        Intent intent = new Intent();
                        intent.setAction(ConstantIds.APPOINTMENT_LIST_CHANGE);
                        intent.putExtra("appointment_item", appointmentItem);
                        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
                    }
                });

    }

    @SuppressLint("SetTextI18n")
    private void populateCurrentDateAndDay() {
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH);
        int date = now.get(Calendar.DAY_OF_MONTH);

        String dayLongName = now.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());
        tvDate.setText(date + " " + getMonthName(month) + "," + " " + year);
        tvDay.setText(dayLongName);
    }

    private String getFormattedDate(long time) {
        Date timeFromDate = new Date(time);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMM, yyyy", Locale.ENGLISH);
        return simpleDateFormat.format(timeFromDate);
    }

    private void notifyTherapistWIthNotification(AppointmentItem appointmentItem) {
        RequestNotificaton requestNotificaton = new RequestNotificaton();
        requestNotificaton.setToken("/topics/" + appointmentItem.getTherapistId());
        requestNotificaton.setSendNotificationModel(new SendNotificationModel("New Appointment!",
                "Dear " + appointmentItem.getTherapistName() + ", you have new appointment with " + appointmentItem.getPatientName()
                        + " at " + getFormattedTime(appointmentItem.getTimeFrom()) + " , " + getFormattedDate(appointmentItem.getDate())));

        WebServicesHandler.instance.sendNotification(requestNotificaton, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) throws JSONException {

            }
        });
    }
}