package com.example.rafiquee.mindcoach.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.rafiquee.mindcoach.R;
import com.example.rafiquee.mindcoach.activities.HireTherapistActivity;
import com.example.rafiquee.mindcoach.models.PatientInfo;
import com.example.rafiquee.mindcoach.models.PatientInfoORM;
import com.example.rafiquee.mindcoach.models.Questionnaire;
import com.example.rafiquee.mindcoach.models.QuestionnaireORM;
import com.example.rafiquee.mindcoach.shared_prefs.SharedPreferencesUtility;
import com.example.rafiquee.mindcoach.utils.ConstantIds;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

@SuppressLint("ValidFragment")
public class SignUpFragment extends BaseFragment {

    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_phone_num)
    EditText etPhone;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.et_confirm_password)
    EditText etConfirmPassword;

    private QuestionnaireORM questionnaireORM;
    private PatientInfoORM patientInfoORM;
    private DatabaseReference databaseReference;
    private ProgressDialog progressDialog;
    private static final int REQUEST_CODE_ALL_PERMISSION = 1323;


    public SignUpFragment() {
    }

    @SuppressLint("ValidFragment")
    public SignUpFragment(QuestionnaireORM questionnaireORM) {
        this.questionnaireORM = questionnaireORM;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        ButterKnife.bind(this, view);

        initializations(view);
        return view;
    }

    private void initializations(View view) {
        patientInfoORM = PatientInfoORM.findById(PatientInfoORM.class, 1L);
        if (patientInfoORM == null)
            patientInfoORM = new PatientInfoORM();
        patientInfoORM.setQuestionnaire(questionnaireORM);

        databaseReference = FirebaseDatabase.getInstance().getReference();
    }

    @OnClick(R.id.btn_sign_up)
    void onBtnSignUpClick() {
        if (isConnectedToInternet()) {
            if (isValid()) {
                patientInfoORM.setUserId(" ");
                patientInfoORM.setName(etName.getText().toString());
                patientInfoORM.setEmail(etEmail.getText().toString());
                patientInfoORM.setPassword(etPassword.getText().toString());
                patientInfoORM.setPhoneNum(etPhone.getText().toString());
                patientInfoORM.setAddress("");
                patientInfoORM.setTherapistId("");
                patientInfoORM.setGender(questionnaireORM.getGender());
                patientInfoORM.setCountry(questionnaireORM.getCountry());

                doSignUp();
            }
        } else
            Snackbar.make(getView(), "Internet connection problem!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
    }

    @OnClick(R.id.tv_login)
    void onTvLoginClick() {
        replaceFragment(new LoginFragment(), null, true);
    }

    private void doSignUp() {

        progressDialog = ProgressDialog.show(getActivity(), "", "Creating Account...");

        //_______________________________ Query to get the all the data of a specific user w.r.t Email _________________________________
        databaseReference.child("patients_list").orderByChild("email").equalTo(etEmail.getText().toString())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            progressDialog.dismiss();
                            Snackbar.make(getView(), "This email is already in use!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        } else {
                            saveDataOnFirebase();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (getActivity() != null) {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), databaseError.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void saveDataOnFirebase() {
        progressDialog.dismiss();

        String id = databaseReference.child("patients_list").push().getKey();
        patientInfoORM.setUserId(id);

        Questionnaire questionnaire = new Questionnaire();
        questionnaire.setGender(patientInfoORM.getQuestionnaire().getGender());
        questionnaire.setAge(patientInfoORM.getQuestionnaire().getAge());
        questionnaire.setReligion(patientInfoORM.getQuestionnaire().getReligion());
        questionnaire.setSuicidePlanTime(patientInfoORM.getQuestionnaire().getSuicidePlanTime());
        questionnaire.setFinancialStatus(patientInfoORM.getQuestionnaire().getFinancialStatus());
        questionnaire.setSleepingHabitsStatus(patientInfoORM.getQuestionnaire().getSleepingHabitsStatus());
        questionnaire.setCountry(patientInfoORM.getQuestionnaire().getCountry());
        questionnaire.setReligious(patientInfoORM.getQuestionnaire().isReligious());
        questionnaire.setHasTherapist(patientInfoORM.getQuestionnaire().isHasTherapist());
        questionnaire.setHasSadnessAndDepression(patientInfoORM.getQuestionnaire().isHasSadnessAndDepression());
        questionnaire.setHasPanicAttacks(patientInfoORM.getQuestionnaire().isHasPanicAttacks());
        questionnaire.setTakingMedication(patientInfoORM.getQuestionnaire().isTakingMedication());
        questionnaire.setHasChronicPain(patientInfoORM.getQuestionnaire().isHasChronicPain());
        questionnaire.setProblemsToTreat(patientInfoORM.getQuestionnaire().getProblemsToTreat());

        PatientInfo patientInfo = new PatientInfo();
        patientInfo.setUserId(id);
        patientInfo.setName(patientInfoORM.getName());
        patientInfo.setEmail(patientInfoORM.getEmail());
        patientInfo.setPassword(patientInfoORM.getPassword());
        patientInfo.setPhoneNum(patientInfoORM.getPhoneNum());
        patientInfo.setAddress(patientInfoORM.getAddress());
        patientInfo.setTherapistId(patientInfoORM.getTherapistId());
        patientInfo.setGender(patientInfoORM.getGender());
        patientInfo.setCountry(patientInfoORM.getCountry());
        patientInfo.setQuestionnaire(questionnaire);


        databaseReference.child("patients_list").child(id).setValue(patientInfo);

        questionnaireORM.save();
        patientInfoORM.save();
        openSweetDialog();
    }

    private boolean isValid() {

        if (TextUtils.isEmpty(etName.getText().toString())) {
            Snackbar.make(getView(), "Please enter name!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            return false;
        } else if (TextUtils.isEmpty(etEmail.getText().toString())) {
            Snackbar.make(getView(), "Please enter email!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            return false;
        } else if (TextUtils.isEmpty(etPhone.getText().toString())) {
            Snackbar.make(getView(), "Please enter phone!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            return false;
        } else if (TextUtils.isEmpty(etPassword.getText().toString())) {
            Snackbar.make(getView(), "Please enter password!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            return false;
        } else if (TextUtils.isEmpty(etConfirmPassword.getText().toString())) {
            Snackbar.make(getView(), "Please confirm password!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            return false;
        }

        //____ email validation ______
        if (!TextUtils.isEmpty(etEmail.getText().toString())) {
            if (!etEmail.getText().toString().contains(".com") || !android.util.Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString()).matches()) {
                Snackbar.make(getView(), "Please enter valid email!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                return false;
            }
        }

        //____ phone validation ______
        if (!TextUtils.isEmpty(etPhone.getText().toString())) {
            String phone = etPhone.getText().toString();

            if (phone.startsWith("64") || phone.startsWith("+1")) {
                if (phone.startsWith("03") && !phone.matches(ConstantIds.CAD_NATIONAL_MOBILE_FORMAT_1) && !phone.matches(ConstantIds.CAD_NATIONAL_MOBILE_FORMAT_2)) {
                    Snackbar.make(getView(), "Please enter valid phone number!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    return false;
                } else if (phone.startsWith("+92") && !phone.matches(ConstantIds.CAD_INTERNATIONAL_MOBILE_FORMAT_1)
                        && !phone.matches(ConstantIds.CAD_INTERNATIONAL_MOBILE_FORMAT_2) && !phone.matches(ConstantIds.CAD_INTERNATIONAL_MOBILE_FORMAT_3)) {
                    Snackbar.make(getView(), "Please enter valid phone number!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    return false;
                }
            } else {
                Snackbar.make(getView(), "Please enter valid phone number!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                return false;
            }
        }

        //____ password validation ______
        if (!TextUtils.isEmpty(etPassword.getText().toString())) {
            String password = etPassword.getText().toString();
            if (password.length() < 6) {
                Snackbar.make(getView(), "Password length should be 6 at least!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                return false;
            }
            if (password.matches("^[a-zA-Z]*$") || password.matches("^[0-9]+$")) {
                Snackbar.make(getView(), "Password should contains digits and alphabets!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                return false;
            }
        }

        if (!TextUtils.equals(etPassword.getText().toString(), etConfirmPassword.getText().toString())) {
            Snackbar.make(getView(), "Password does not match!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            return false;
        }

        return true;
    }

    private void openSweetDialog() {
        new SweetAlertDialog(getContext(), SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Registration Completed!")
                .setConfirmText("Ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        SharedPreferencesUtility.setPreference(getActivity(), ConstantIds.IS_PATIENT_LOGGED_IN, true);
                        getAllPermissions();
                    }
                })
                .show();
    }

    private void getAllPermissions() {
        String[] PERMISSIONS = {
                Manifest.permission.WRITE_CONTACTS,
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.INTERNET,
                android.Manifest.permission.SEND_SMS,
        };

        if (!hasPermissions(getActivity(), PERMISSIONS)) {
            requestPermissions(PERMISSIONS, REQUEST_CODE_ALL_PERMISSION);
        } else {
            Intent intent = new Intent(getActivity(), HireTherapistActivity.class);
            intent.putExtra("tag_from", "SignUpFragment");
            startActivity(intent);
            getActivity().finish();
        }
    }

    public boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ALL_PERMISSION:
                for (int permission : grantResults) {
                    if (permission == PackageManager.PERMISSION_DENIED) {
                        return;
                    }
                }

                Intent intent = new Intent(getActivity(), HireTherapistActivity.class);
                intent.putExtra("tag_from", "SignUpFragment");
                startActivity(intent);
                getActivity().finish();
                break;
        }
    }
}
