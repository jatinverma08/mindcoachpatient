package com.example.rafiquee.mindcoach.fragments.questionnaire;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.rafiquee.mindcoach.R;
import com.example.rafiquee.mindcoach.fragments.BaseFragment;
import com.example.rafiquee.mindcoach.models.Questionnaire;
import com.example.rafiquee.mindcoach.models.QuestionnaireORM;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.galaxyofandroid.spinerdialog.OnSpinerItemClick;
import in.galaxyofandroid.spinerdialog.SpinnerDialog;

@SuppressLint("ValidFragment")
public class AgeSelectionFragment extends BaseFragment {

    @BindView(R.id.et_age)
    EditText etAge;
    private QuestionnaireORM questionnaireORM;

    @SuppressLint("ValidFragment")
    public AgeSelectionFragment(QuestionnaireORM questionnaireORM) {
        this.questionnaireORM = questionnaireORM;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_age_selection, container, false);
        ButterKnife.bind(this, view);

        initializations();
        return view;
    }

    private void initializations() {
    }

    @OnClick(R.id.et_age)
    void onEtCountryClick() {
        final ArrayList<String> ageList = new ArrayList<>();
        for (int i = 5; i <= 100; i++)
            ageList.add(String.valueOf(i));

        SpinnerDialog spinnerDialog = new SpinnerDialog(getActivity(), ageList, "Select your age:", R.style.DialogAnimations_SmileWindow, "Close");
        spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String item, int position) {
                etAge.setText(ageList.get(position));
                questionnaireORM.setAge(ageList.get(position));
            }
        });

        spinnerDialog.showSpinerDialog();
    }

    @OnClick(R.id.btn_next)
    void onNextCLick() {
        if (!TextUtils.isEmpty(etAge.getText().toString()))
            replaceFragment(new ReligiousAskingFragment(questionnaireORM), null, true);
        else
            Toast.makeText(getActivity(), "Please select your age first!", Toast.LENGTH_SHORT).show();
    }
}