package com.example.rafiquee.mindcoach.fragments.questionnaire;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rafiquee.mindcoach.R;
import com.example.rafiquee.mindcoach.fragments.BaseFragment;
import com.example.rafiquee.mindcoach.models.Questionnaire;
import com.example.rafiquee.mindcoach.models.QuestionnaireORM;

import butterknife.ButterKnife;
import butterknife.OnClick;

@SuppressLint("ValidFragment")
public class AlreadyInTherapyAskingFragment extends BaseFragment {

    private QuestionnaireORM questionnaireORM;

    @SuppressLint("ValidFragment")
    public AlreadyInTherapyAskingFragment(QuestionnaireORM questionnaireORM) {
        this.questionnaireORM = questionnaireORM;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_already_in_therapy_asking, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @OnClick(R.id.btn_yes)
    void onBtnYesClick() {
        questionnaireORM.setHasTherapist(true);
        replaceFragment(new SadnessAndDepressionAskingFragment(questionnaireORM), null,true);
    }

    @OnClick(R.id.btn_no)
    void onBtnNoClick() {
        questionnaireORM.setHasTherapist(false);
        replaceFragment(new SadnessAndDepressionAskingFragment(questionnaireORM), null,true);
    }
}
