package com.example.rafiquee.mindcoach.fragments.questionnaire;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.rafiquee.mindcoach.R;
import com.example.rafiquee.mindcoach.fragments.BaseFragment;
import com.example.rafiquee.mindcoach.models.Questionnaire;
import com.example.rafiquee.mindcoach.models.QuestionnaireORM;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.galaxyofandroid.spinerdialog.OnSpinerItemClick;
import in.galaxyofandroid.spinerdialog.SpinnerDialog;

@SuppressLint("ValidFragment")
public class CountryAskingFragment extends BaseFragment {

    @BindView(R.id.et_country)
    EditText etCountry;

    private ArrayList<String> countriesList = new ArrayList<>();
    private QuestionnaireORM questionnaireORM;


    @SuppressLint("ValidFragment")
    public CountryAskingFragment(QuestionnaireORM questionnaireORM) {
        this.questionnaireORM = questionnaireORM;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_country_asking, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.btn_next)
    void onNextClick() {
        if (!TextUtils.isEmpty(etCountry.getText().toString()))
            replaceFragment(new TherapistExperienceSelectionFragment(questionnaireORM), null, true);
        else
            Toast.makeText(getActivity(), "Please select your country first!", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.et_country)
    void onEtCountryClick() {
        countriesList.clear();
        String[] countryCodes = Locale.getISOCountries();
        for (String countryCode : countryCodes) {
            Locale obj = new Locale("", countryCode);
            countriesList.add(obj.getDisplayCountry());
        }

        SpinnerDialog spinnerDialog = new SpinnerDialog(getActivity(), countriesList, "Select your country:", R.style.DialogAnimations_SmileWindow, "Close");

        spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String item, int position) {
                etCountry.setText(countriesList.get(position));
                questionnaireORM.setCountry(countriesList.get(position));
            }
        });

        spinnerDialog.showSpinerDialog();
    }
}
