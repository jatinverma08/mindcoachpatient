package com.example.rafiquee.mindcoach.fragments.questionnaire;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rafiquee.mindcoach.R;
import com.example.rafiquee.mindcoach.fragments.BaseFragment;
import com.example.rafiquee.mindcoach.models.Questionnaire;
import com.example.rafiquee.mindcoach.models.QuestionnaireORM;

import butterknife.ButterKnife;
import butterknife.OnClick;

@SuppressLint("ValidFragment")
public class FinancialStatusAskingFragment extends BaseFragment {

    private QuestionnaireORM questionnaireORM;

    @SuppressLint("ValidFragment")
    public FinancialStatusAskingFragment(QuestionnaireORM questionnaireORM) {
        this.questionnaireORM = questionnaireORM;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_financial_status_asking, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @OnClick(R.id.btn_good)
    void onBtnGoodClick() {
        questionnaireORM.setFinancialStatus(getString(R.string.good));
        replaceFragment(new SleepingHabitsAskingFragment(questionnaireORM), null, true);
    }

    @OnClick(R.id.btn_fair)
    void onBtnFairClick() {
        questionnaireORM.setFinancialStatus(getString(R.string.fair));
        replaceFragment(new SleepingHabitsAskingFragment(questionnaireORM), null, true);
    }

    @OnClick(R.id.btn_poor)
    void onBtnPoorClick() {
        questionnaireORM.setFinancialStatus(getString(R.string.poor));
        replaceFragment(new SleepingHabitsAskingFragment(questionnaireORM), null, true);
    }
}
