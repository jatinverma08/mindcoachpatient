package com.example.rafiquee.mindcoach.fragments.questionnaire;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rafiquee.mindcoach.R;
import com.example.rafiquee.mindcoach.fragments.BaseFragment;
import com.example.rafiquee.mindcoach.models.Questionnaire;
import com.example.rafiquee.mindcoach.models.QuestionnaireORM;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class GenderSelectionFragment extends BaseFragment {

    private QuestionnaireORM questionnaireORM;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gender_selection, container, false);
        ButterKnife.bind(this, view);

        initializations(view);
        return view;
    }

    private void initializations(View view) {
        questionnaireORM = new QuestionnaireORM();
    }

    @OnClick(R.id.btn_male)
    void OnBtnMaleClick() {
        questionnaireORM.setGender(getString(R.string.male));
        replaceFragment(new AgeSelectionFragment(questionnaireORM), null, true);
    }

    @OnClick(R.id.btn_female)
    void OnBtnFemaleClick() {
        questionnaireORM.setGender(getString(R.string.female));
        replaceFragment(new AgeSelectionFragment(questionnaireORM), null, true);
    }

    @OnClick(R.id.btn_non_binary)
    void OnBtnNonBinaryClick() {
        questionnaireORM.setGender(getString(R.string.non_binary));
        replaceFragment(new AgeSelectionFragment(questionnaireORM), null, true);
    }
}
