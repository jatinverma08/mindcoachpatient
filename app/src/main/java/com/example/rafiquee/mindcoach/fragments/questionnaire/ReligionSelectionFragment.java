package com.example.rafiquee.mindcoach.fragments.questionnaire;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rafiquee.mindcoach.R;
import com.example.rafiquee.mindcoach.fragments.BaseFragment;
import com.example.rafiquee.mindcoach.models.Questionnaire;
import com.example.rafiquee.mindcoach.models.QuestionnaireORM;

import butterknife.ButterKnife;
import butterknife.OnClick;

@SuppressLint("ValidFragment")
public class ReligionSelectionFragment extends BaseFragment {

    private QuestionnaireORM questionnaireORM;

    @SuppressLint("ValidFragment")
    public ReligionSelectionFragment(QuestionnaireORM questionnaireORM) {
        this.questionnaireORM = questionnaireORM;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_religion_selection, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.btn_islam)
    void onBtnIslamClick() {
        questionnaireORM.setReligion(getString(R.string.islam));
        replaceFragment(new AlreadyInTherapyAskingFragment(questionnaireORM), null, true);
    }

    @OnClick(R.id.btn_christianity)
    void onBtnChristianityClick() {
        questionnaireORM.setReligion(getString(R.string.christianity));
        replaceFragment(new AlreadyInTherapyAskingFragment(questionnaireORM), null, true);
    }

    @OnClick(R.id.btn_judaism)
    void onBtnJewClick() {
        questionnaireORM.setReligion(getString(R.string.judaism));
        replaceFragment(new AlreadyInTherapyAskingFragment(questionnaireORM), null, true);
    }

    @OnClick(R.id.btn_other_religion)
    void onBtnOtherReligionClick() {
        questionnaireORM.setReligion(getString(R.string.other));
        replaceFragment(new AlreadyInTherapyAskingFragment(questionnaireORM), null, true);
    }
}
