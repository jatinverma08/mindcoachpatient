package com.example.rafiquee.mindcoach.fragments.questionnaire;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rafiquee.mindcoach.R;
import com.example.rafiquee.mindcoach.fragments.BaseFragment;
import com.example.rafiquee.mindcoach.models.Questionnaire;
import com.example.rafiquee.mindcoach.models.QuestionnaireORM;

import butterknife.ButterKnife;
import butterknife.OnClick;

@SuppressLint("ValidFragment")
public class ReligiousAskingFragment extends BaseFragment {

    private QuestionnaireORM questionnaireORM;

    @SuppressLint("ValidFragment")
    public ReligiousAskingFragment(QuestionnaireORM questionnaireORM) {
        this.questionnaireORM = questionnaireORM;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_religious_asking, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.btn_yes)
    void onBtnYesClick() {
        questionnaireORM.setReligious(true);
        replaceFragment(new ReligionSelectionFragment(questionnaireORM), null,true);
    }

    @OnClick(R.id.btn_no)
    void onBtnNoClick() {
        questionnaireORM.setReligious(false);
        replaceFragment(new AlreadyInTherapyAskingFragment(questionnaireORM), null,true);
    }
}
