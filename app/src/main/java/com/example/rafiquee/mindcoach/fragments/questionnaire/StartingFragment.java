package com.example.rafiquee.mindcoach.fragments.questionnaire;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.rafiquee.mindcoach.R;
import com.example.rafiquee.mindcoach.activities.TipsActivity;
import com.example.rafiquee.mindcoach.fragments.BaseFragment;
import com.example.rafiquee.mindcoach.fragments.LoginFragment;
import com.google.firebase.messaging.FirebaseMessaging;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class StartingFragment extends BaseFragment {

    @BindView(R.id.btn_vr_hypnotherapy)
    Button btnVRHypnotherapy;
    @BindView(R.id.btn_counseling_from_therapist)
    Button btnCounselingFromTherapist;
    @BindView(R.id.btn_tips)
    Button btnTips;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = getLayoutInflater().inflate(R.layout.fragment_starting, container, false);

        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.btn_vr_hypnotherapy)
    void onBtnVRHypnotherapyClick() {
        replaceFragment(new GenderSelectionFragment(),null, true);
    }

    @OnClick(R.id.btn_counseling_from_therapist)
    void onBtnCounselingFromTherapistClick() {
        replaceFragment(new GenderSelectionFragment(),null, true);
    }

    @OnClick(R.id.btn_tips)
    void onBtnTipsClick() {
        startActivity(new Intent(getActivity(), TipsActivity.class));
    }

    @OnClick(R.id.tv_login)
    void onTvLoginClick() {
        replaceFragment(new LoginFragment(), null, true);
    }
}
