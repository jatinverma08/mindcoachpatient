package com.example.rafiquee.mindcoach.fragments.questionnaire;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rafiquee.mindcoach.R;
import com.example.rafiquee.mindcoach.fragments.BaseFragment;
import com.example.rafiquee.mindcoach.models.Questionnaire;
import com.example.rafiquee.mindcoach.models.QuestionnaireORM;

import butterknife.ButterKnife;
import butterknife.OnClick;

@SuppressLint("ValidFragment")
public class SuicideAskingFragment extends BaseFragment {

    private QuestionnaireORM questionnaireORM;

    @SuppressLint("ValidFragment")
    public SuicideAskingFragment(QuestionnaireORM questionnaireORM) {
        this.questionnaireORM = questionnaireORM;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_suicide_asking, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.btn_never)
    void onBtnNeverClick() {
        questionnaireORM.setSuicidePlanTime(getString(R.string.never));
        replaceFragment(new PanicAttacksAskingFragment(questionnaireORM), null, true);
    }

    @OnClick(R.id.btn_a_year_ago)
    void onBtnAYearAgoClick() {
        questionnaireORM.setSuicidePlanTime(getString(R.string.over_a_year_ago));
        replaceFragment(new PanicAttacksAskingFragment(questionnaireORM), null, true);
    }

    @OnClick(R.id.btn_three_months_ago)
    void onBtnThreeMonthsAgoClick() {
        questionnaireORM.setSuicidePlanTime(getString(R.string.over_three_months_ago));
        replaceFragment(new PanicAttacksAskingFragment(questionnaireORM), null, true);
    }

    @OnClick(R.id.btn_a_month_ago)
    void onBtnAMonthAgoClick() {
        questionnaireORM.setSuicidePlanTime(getString(R.string.over_a_month_ago));
        replaceFragment(new PanicAttacksAskingFragment(questionnaireORM), null, true);
    }

    @OnClick(R.id.btn_a_week_ago)
    void onBtnAWeekAgoClick() {
        questionnaireORM.setSuicidePlanTime(getString(R.string.over_a_week_ago));
        replaceFragment(new PanicAttacksAskingFragment(questionnaireORM), null, true);
    }

    @OnClick(R.id.btn_this_week)
    void onBtnThisWeekClick() {
        questionnaireORM.setSuicidePlanTime(getString(R.string.this_week));
        replaceFragment(new PanicAttacksAskingFragment(questionnaireORM), null, true);
    }
}
