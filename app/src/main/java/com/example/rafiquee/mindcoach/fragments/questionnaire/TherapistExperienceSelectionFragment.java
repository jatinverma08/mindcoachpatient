package com.example.rafiquee.mindcoach.fragments.questionnaire;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.example.rafiquee.mindcoach.R;
import com.example.rafiquee.mindcoach.fragments.BaseFragment;
import com.example.rafiquee.mindcoach.fragments.SignUpFragment;
import com.example.rafiquee.mindcoach.models.QuestionnaireORM;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

@SuppressLint("ValidFragment")
public class TherapistExperienceSelectionFragment extends BaseFragment {

    @BindView(R.id.cb_depression)
    CheckBox cbDepression;

    private QuestionnaireORM questionnaireORM;
    private ArrayList<String> selectedTherapistExperiencesList;


    @SuppressLint("ValidFragment")
    public TherapistExperienceSelectionFragment(QuestionnaireORM questionnaireORM) {
        this.questionnaireORM = questionnaireORM;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = getLayoutInflater().inflate(R.layout.fragment_therapist_experience_selection, container, false);
        ButterKnife.bind(this, view);

        initializations(view);
        return view;
    }

    private void initializations(View view) {
        selectedTherapistExperiencesList = new ArrayList<>();
    }

    @OnClick(R.id.btn_next)
    void onBtnNextClick() {
        if (selectedTherapistExperiencesList.size() != 0) {
            String problems = "";
            for (int i = 0; i < selectedTherapistExperiencesList.size(); i++)
                problems = problems + selectedTherapistExperiencesList.get(i) + ", ";
            questionnaireORM.setProblemsToTreat(problems);
            openSweetDialog();
        } else
            Toast.makeText(getActivity(), "Please make one selection at least!", Toast.LENGTH_SHORT).show();
    }

    @OnCheckedChanged(R.id.cb_depression)
    void setCbDepression(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.depression));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.depression));
    }

    @OnCheckedChanged(R.id.cb_stress_and_anxiety)
    void setCbStress(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.stress_and_anxiety));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.stress_and_anxiety));
    }

    @OnCheckedChanged(R.id.cb_coping_with_addictions)
    void setCbAddiction(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.coping_with_addictions));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.coping_with_addictions));
    }

    @OnCheckedChanged(R.id.cb_lgbt_related_issues)
    void setCbLGBTIssues(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.lgbt_related_issues));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.lgbt_related_issues));
    }

    @OnCheckedChanged(R.id.cb_relationship_issues)
    void setCbRelationship(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.relationship_issues));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.relationship_issues));
    }

    @OnCheckedChanged(R.id.cb_family_conflicts)
    void setCbFamilyConflicts(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.family_conflicts));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.family_conflicts));
    }

    @OnCheckedChanged(R.id.cb_trauma_and_abuse)
    void setCbTrauma(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.trauma_and_abuse));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.trauma_and_abuse));
    }

    @OnCheckedChanged(R.id.cb_coping_with_grief)
    void setCbGrief(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.coping_with_grief_and_loss));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.coping_with_grief_and_loss));
    }

    @OnCheckedChanged(R.id.cb_intimacy_related_issues)
    void setCbIntimacyIssues(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.intimacy_related_issues));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.intimacy_related_issues));
    }

    @OnCheckedChanged(R.id.cb_eating_disorder)
    void setCbEatingDisorder(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.eating_disorder));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.eating_disorder));
    }

    @OnCheckedChanged(R.id.cb_sleeping_disorder)
    void setCbSleepingDisorder(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.sleeping_disorder));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.sleeping_disorder));
    }

    @OnCheckedChanged(R.id.cb_parenting_issues)
    void setCbParentingIssues(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.parenting_issues));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.parenting_issues));
    }

    @OnCheckedChanged(R.id.cb_motivation)
    void setCbMotivation(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.motivation_self_esteem_and_confidence));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.motivation_self_esteem_and_confidence));
    }

    @OnCheckedChanged(R.id.cb_anger_management)
    void setCbAngerManagement(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.anger_management));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.anger_management));
    }

    @OnCheckedChanged(R.id.cb_career_difficulties)
    void setCbCareerDifficulties(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.career_difficulties));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.career_difficulties));
    }

    @OnCheckedChanged(R.id.cb_bipolar_disorder)
    void setCbBipolarDisorder(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.bipolar_disorder));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.bipolar_disorder));
    }

    @OnCheckedChanged(R.id.cb_life_changes)
    void setCbLifeChanges(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.coping_with_life_changes));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.coping_with_life_changes));
    }

    @OnCheckedChanged(R.id.cb_professional_coaching)
    void setCbProfessionalCoaching(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.exclusive_and_professional_coaching));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.exclusive_and_professional_coaching));
    }

    @OnCheckedChanged(R.id.cb_compassion_fatigue)
    void setCbCompassionFatigue(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.compassion_fatigue));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.compassion_fatigue));
    }

    @OnCheckedChanged(R.id.cb_concentration)
    void setCbConcentration(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            selectedTherapistExperiencesList.add(getString(R.string.concentration_memory_and_focus_adhd));
        else
            selectedTherapistExperiencesList.remove(getString(R.string.concentration_memory_and_focus_adhd));
    }


    private void openSweetDialog() {
        new SweetAlertDialog(getContext(), SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Questionnaire Completed!")
                .setContentText("Now go for the Sign Up!")
                .setConfirmText("Ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        replaceFragment(new SignUpFragment(questionnaireORM), null, true);
                    }
                }).show();
    }
}
