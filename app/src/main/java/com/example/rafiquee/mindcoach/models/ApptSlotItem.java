package com.example.rafiquee.mindcoach.models;

public class ApptSlotItem {

    private long timeFrom, timeTo;

    public ApptSlotItem() {
    }

    public long getTimeFrom() {
        return timeFrom;
    }

    public ApptSlotItem(long timeFrom, long timeTo) {
        this.timeFrom = timeFrom;
        this.timeTo = timeTo;
    }

    public void setTimeFrom(long timeFrom) {
        this.timeFrom = timeFrom;
    }

    public long getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(long timeTo) {
        this.timeTo = timeTo;
    }
}