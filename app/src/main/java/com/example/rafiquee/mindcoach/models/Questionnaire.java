package com.example.rafiquee.mindcoach.models;

import com.orm.SugarRecord;

import java.util.ArrayList;

public class Questionnaire {

    private String gender, age, religion, suicidePlanTime, financialStatus, sleepingHabitsStatus, country, problemsToTreat;
    private boolean isReligious, hasTherapist, hasSadnessAndDepression, hasPanicAttacks, takingMedication, hasChronicPain;

    public Questionnaire() {
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getSuicidePlanTime() {
        return suicidePlanTime;
    }

    public void setSuicidePlanTime(String suicidePlanTime) {
        this.suicidePlanTime = suicidePlanTime;
    }

    public String getFinancialStatus() {
        return financialStatus;
    }

    public void setFinancialStatus(String financialStatus) {
        this.financialStatus = financialStatus;
    }

    public boolean isHasPanicAttacks() {
        return hasPanicAttacks;
    }

    public void setHasPanicAttacks(boolean hasPanicAttacks) {
        this.hasPanicAttacks = hasPanicAttacks;
    }

    public String getSleepingHabitsStatus() {
        return sleepingHabitsStatus;
    }

    public void setSleepingHabitsStatus(String sleepingHabitsStatus) {
        this.sleepingHabitsStatus = sleepingHabitsStatus;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public boolean isReligious() {
        return isReligious;
    }

    public void setReligious(boolean religious) {
        isReligious = religious;
    }

    public boolean isHasTherapist() {
        return hasTherapist;
    }

    public void setHasTherapist(boolean hasTherapist) {
        this.hasTherapist = hasTherapist;
    }

    public boolean isHasSadnessAndDepression() {
        return hasSadnessAndDepression;
    }

    public void setHasSadnessAndDepression(boolean hasSadnessAndDepression) {
        this.hasSadnessAndDepression = hasSadnessAndDepression;
    }

    public boolean isTakingMedication() {
        return takingMedication;
    }

    public void setTakingMedication(boolean takingMedication) {
        this.takingMedication = takingMedication;
    }

    public boolean isHasChronicPain() {
        return hasChronicPain;
    }

    public void setHasChronicPain(boolean hasChronicPain) {
        this.hasChronicPain = hasChronicPain;
    }

    public String getProblemsToTreat() {
        return problemsToTreat;
    }

    public void setProblemsToTreat(String problemsToTreat) {
        this.problemsToTreat = problemsToTreat;
    }
}
