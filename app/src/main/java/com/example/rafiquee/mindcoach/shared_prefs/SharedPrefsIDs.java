package com.example.rafiquee.mindcoach.shared_prefs;

public class SharedPrefsIDs {

    public static String PHONE_NUM_VALIDATED = "PHONE_NUM_VALIDATED";
    public static String DEVICE_ID = "DEVICE_ID";
    public static String ACCOUNT_NUMBER = "ACCOUNT_NUMBER";
    public static String APP_IS_IN_USE = "APP_IS_IN_USE";
}
