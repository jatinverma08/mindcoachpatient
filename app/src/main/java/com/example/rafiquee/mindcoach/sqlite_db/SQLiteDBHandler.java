package com.example.rafiquee.mindcoach.sqlite_db;

/**
 * Created by Ehsan Ullah on 12/17/2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;

import java.util.ArrayList;

public class SQLiteDBHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "true_caller_db";
    private static final String PREFS_TABLE = "tbl_prefs";
    private Context context;


    public SQLiteDBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable1 = "CREATE TABLE " + PREFS_TABLE + "(contacts_synced BOOLEAN)";
        db.execSQL(createTable1);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + PREFS_TABLE);
        this.onCreate(db);
    }


    public void contactsSynced(boolean synced) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("contacts_synced", synced);

        db.insert(PREFS_TABLE, null, values);
        db.close();
    }

    public Boolean isContactsSynced() {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("Select contacts_synced from " + PREFS_TABLE , null);
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }

        cursor.close();
        return true;
    }
}

