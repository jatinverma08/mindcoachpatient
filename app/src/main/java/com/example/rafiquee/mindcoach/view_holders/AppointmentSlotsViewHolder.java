package com.example.rafiquee.mindcoach.view_holders;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rafiquee.mindcoach.R;
import com.example.rafiquee.mindcoach.interfaces.CustomOnClick;
import com.example.rafiquee.mindcoach.models.AppointmentItem;
import com.example.rafiquee.mindcoach.models.ApptSlotItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AppointmentSlotsViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.btn_appt_slot)
    public Button btnApptSLot;


    private ArrayList<ApptSlotItem> apptSlotsList;
    private CustomOnClick customOnClick;

    public AppointmentSlotsViewHolder(final View itemView, ArrayList<ApptSlotItem> apptSlotsList, final CustomOnClick customOnClick) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.apptSlotsList = apptSlotsList;
        this.customOnClick = customOnClick;

        itemView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                customOnClick.onClick(itemView, getAdapterPosition());
            }
        });
    }
}
