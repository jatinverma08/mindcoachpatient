package com.example.rafiquee.mindcoach.view_holders;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.rafiquee.mindcoach.R;
import com.example.rafiquee.mindcoach.interfaces.CustomOnClick;
import com.example.rafiquee.mindcoach.models.AppointmentItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AppointmentsListViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tv_name)
    public TextView tvName;
    @BindView(R.id.tv_phone_num)
    public TextView tvPhone;
    @BindView(R.id.tv_appt_date)
    public TextView tvApptDate;
    @BindView(R.id.tv_appt_duration)
    public TextView tvApptDuration;
    @BindView(R.id.tv_address)
    public TextView tvAddress;
    @BindView(R.id.tv_status)
    public TextView tvStatus;

    private ArrayList<AppointmentItem> appointmentsList;
    private CustomOnClick customOnClick;


    public AppointmentsListViewHolder(final View itemView, ArrayList<AppointmentItem> appointmentsList, final CustomOnClick customOnClick) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.appointmentsList = appointmentsList;
        this.customOnClick = customOnClick;

        itemView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                customOnClick.onClick(itemView, getAdapterPosition());
            }
        });
    }
}


