package com.example.rafiquee.mindcoach.view_holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.rafiquee.mindcoach.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AvailableDaysViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tv_day)
    public TextView tvDay;


    private ArrayList<String> availableDaysList;

    public AvailableDaysViewHolder(final View itemView, ArrayList<String> availableDaysList) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.availableDaysList = availableDaysList;
    }
}